#include <cstring>
#include <QDebug>
#include <QLoggingCategory>
#include <QSslSocket>
#include <QStringBuilder>
#include <cxirc/irc_proto.hpp>

using namespace cxirc;
Q_LOGGING_CATEGORY(logIrcProto, "IrcProto")

IrcProto::IrcProto(QObject *parent)
    : QObject(parent)
    , _state(PState::Disconnected)
{
}

void IrcProto::setAuthentication(cxirc::IrcClientAuthentication auth)
{
    _auth = auth;
}

void IrcProto::connectToHost(const IrcConnectionData& connection)
{
    if (_state != PState::Disconnected) {
        _socket->abort();
    }
    _state = PState::Connecting;
    if (connection.tls) {
        auto socket = new QSslSocket(this);
        connect(socket, &QSslSocket::encrypted, this, &IrcProto::socketReady);
        socket->connectToHostEncrypted(connection.host, connection.port.value_or(6697));
        setSocketCore(*socket);
    } else {
        auto socket = new QTcpSocket(this);
        connect(socket, &QTcpSocket::connected, this, &IrcProto::socketReady);
        socket->connectToHost(connection.host, connection.port.value_or(6667));
        setSocketCore(*socket);
    }
}

void IrcProto::setSocketCore(QAbstractSocket& socket) {
    _socket = &socket;
    connect(_socket, &QTcpSocket::readyRead, this, &IrcProto::readyRead);
    connect(_socket, &QTcpSocket::disconnected, this, &IrcProto::socketDisconnected);
}

void IrcProto::setSocket(QAbstractSocket& socket)
{
    if (_state != PState::Disconnected) {
        _socket->abort();
    }
    setSocketCore(socket);
    socketReady();
}

void IrcProto::socketReady()
{
    if (_auth) {
        if (_auth->pass.has_value()) {
            write(QStringLiteral("PASS %1").arg(*_auth->pass));
        }
        write(QStringLiteral("NICK %1").arg(_auth->nickname));
        write(QStringLiteral("USER %1 :%2")
            .arg(_auth->username.value_or(_auth->nickname))
            .arg(_auth->realname.value_or(_auth->nickname)));
    }
    _state = PState::Ready;
    emit connected();
}

void IrcProto::socketDisconnected()
{
    qCDebug(logIrcProto) << "socket disconnected";
    emit disconnected();
}

void IrcProto::send(const IrcMessage& msg)
{
    write(msg.serialize());
}

void IrcProto::write(const QString &str)
{
    qCDebug(logIrcProto) << "write" << str;
    _socket->write(str.toUtf8());
    _socket->write("\r\n");
}

void IrcProto::readyRead()
{
    _lineBuffer.readLines(
        [this](char *buffer, int len) { return _socket->read(buffer, len); },
        [this](char *start, int len) { onRead(QString::fromUtf8(start, len)); });
}

void IrcProto::onRead(const QString& str)
{
    qCDebug(logIrcProto) << "read" << str;
    auto msg = IrcMessage::parse(str);
    if (msg.command == QStringLiteral("PING")) {
        IrcMessage reply;
        reply.command = QStringLiteral("PONG");
        reply.params = msg.params;
        reply.colon = true;
        send(reply);
    } else {
        emit messageReceived(msg);
    }
}

static int space(const QStringView& str, int from)
{
    auto res = str.indexOf(' ', from);
    if (res == -1) res = str.size();
    return res;
}
IrcMessage IrcMessage::parse(const QString& in)
{
    cxirc::IrcMessage msg;
    int i = 0;
    auto str = QStringView {in};
    if (str.size() && str[0] == ':') {
        i = space(str, i);
        msg.prefix = str.mid(1, i-1);
        i++;
    }
    if (i < str.size()) {
        auto end = space(str, i);
        msg.command = str.mid(i, end-i);
        i = end+1;
    }
    while (i < str.size()) {
        auto end = str[i] == ':' ? str.size() : space(str, i);
        msg.params.push_back(str.mid(i, end-i));
        i = end+1;
    }
    return msg;
}

QString IrcMessage::serialize() const
{
    QString result;
    auto size = prefix.size();
    if (size) size += 2;
    size += command.size();
    for (auto& param : params) {
        size += param.size() + 1;
    }
    if (colon) size++;
    result.reserve(size);

    if (!prefix.isNull()) {
        result.append(':');
        result.append(prefix);
        result.append(' ');
    }
    result.append(command);
    for (auto i=0; i<params.size(); i++) {
        if (colon && i == params.size()-1) {
            result.append(':');
        }
        result.append(params[i]);
    }
    return result;
}
