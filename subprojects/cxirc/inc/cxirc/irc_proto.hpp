#pragma once
#include <memory>
#include <optional>
#include <QAbstractSocket>
#include <QObject>
#include <QString>
#include "line_buffer.hpp"

namespace cxirc {
    struct IrcClientAuthentication {
        QString nickname;
        std::optional<QString> username;
        std::optional<QString> realname;
        std::optional<QString> pass;
    };

    struct IrcConnectionData {
        QString host;
        std::optional<quint16> port;
        bool tls;
    };

    struct IrcMessage {
        // without the leading :
        QStringView prefix;
        QStringView command;
        // last param without the leading :
        QVector<QStringView> params;
        // if true, send last param with colon. Set this for PRIVMSG, QUIT, PART, etc.
        bool colon;

        QString serialize() const;
        static IrcMessage parse(const QString& str);
    };

    // Low-level IRC protocol client.
    // 1. Connect to the connected/disconnected/messageReceived signals
    // 2. Call setAuthentication()
    // 3. Call either connectToHost() or setSocket()
    //    if calling setSocket, the socket must be already connected, and the caller is responsible for destroying it.
    // 4. Inbound messages can be read using the messageReceived() signal,
    //    outbound message can be sent using the send() method
    class IrcProto : public QObject {
        Q_OBJECT;
        enum class PState {
            Disconnected,
            Connecting,
            Ready,
            Disconnecting,
        };
        PState _state;
        QAbstractSocket* _socket;
        std::optional<IrcClientAuthentication> _auth;
        LineBuffer<512> _lineBuffer;

        void write(const QString& str);
        void onRead(const QString& str);
        void setSocketCore(QAbstractSocket& socket);
    private slots:
        void socketReady();
        void socketDisconnected();
        void readyRead();
    public:
        explicit IrcProto(QObject *parent = nullptr);
        void setAuthentication(IrcClientAuthentication auth);
        void connectToHost(const IrcConnectionData& connection);
        void setSocket(QAbstractSocket& socket);
        void send(const IrcMessage& msg);
    signals:
        void connected();
        void disconnected();
        void messageReceived(const IrcMessage& msg);
    };
}
