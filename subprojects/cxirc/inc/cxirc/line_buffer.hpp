#pragma once
#include <array>
#include <string.h>

namespace cxirc {
    template<size_t N>
    class LineBuffer {
        std::array<char, N> _buffer;
        size_t _bytesBuffered;
        size_t _bytesConsumed;
        size_t _discardLine;
    public:
        template<typename Reader, typename Handler>
        inline void readLines(const Reader& reader, const Handler& handler)
        {
            auto d = _buffer.data();
            while (true) {
                size_t bytesRead = reader(d, _buffer.size() - _bytesBuffered);
                if (bytesRead == 0) {
                    return;
                }

                _bytesBuffered += bytesRead;
                char* linePosition = nullptr;
                do {
                    auto lineStart = d + _bytesConsumed,
                    linePosition = static_cast<char*>(memmem(
                        lineStart,
                        _bytesBuffered - _bytesConsumed,
                        "\r\n", 2));
                    if (linePosition) {
                        int lineLength = linePosition - lineStart;
                        if (!_discardLine) {
                            handler(lineStart, lineLength);
                        }
                        _bytesConsumed += lineLength;
                        _discardLine = false;
                    }
                } while (linePosition);

                size_t bytesRemaining = _bytesBuffered - _bytesConsumed;
                if (bytesRemaining == _buffer.size()) {
                    // line too long
                    _discardLine = true;
                    _bytesBuffered = 0;
                    _bytesConsumed = 0;
                } else if (bytesRemaining > 0 && _bytesConsumed > 0) {
                    // move the last partial message to the front of the buffer, so a full-sized
                    // message will fit
                    memmove(d, d+_bytesConsumed, bytesRemaining);
                    _bytesBuffered = bytesRemaining;
                    _bytesConsumed = 0;
                }
            }
        }
    };
}
