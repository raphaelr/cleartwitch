#include <QCoreApplication>
#include <QCommandLineParser>
#include <cstdio>
#include <cxirc/irc_proto.hpp>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setOrganizationName("tape software");
    QCoreApplication::setOrganizationDomain("tapesoftware.net");
    QCoreApplication::setApplicationName("cxirc");

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("host", "Hostname to connect to. Required.");
    parser.addPositionalArgument("nick", "Nickname to use. Required.");
    QCommandLineOption tlsOption({"t", "tls"}, "Use TLS to connect");
    parser.addOption(tlsOption);
    QCommandLineOption portOption({"p", "port"}, "Overrides the tcp port to use", "port");
    parser.addOption(portOption);

    parser.process(app);
    auto args = parser.positionalArguments();
    if (args.size() != 2) {
        std::fputs(qPrintable(parser.helpText()), stderr);
        return 1;
    }
    cxirc::IrcConnectionData connectionData;
    cxirc::IrcClientAuthentication auth;

    connectionData.host = args[0];
    auth.nickname = args[1];
    if (parser.isSet(tlsOption)) { connectionData.tls = true; }
    if (parser.isSet(portOption)) { connectionData.port = static_cast<quint16>(parser.value(portOption).toInt()); }
    auto pass = getenv("CXIRC_PASS");
    if (pass) {
        auth.pass = pass;
    }

    cxirc::IrcProto proto;
    QObject::connect(&proto, &cxirc::IrcProto::disconnected, [&app]() { app.quit(); });
    proto.setAuthentication(auth);
    proto.connectToHost(connectionData);
    return app.exec();
}
