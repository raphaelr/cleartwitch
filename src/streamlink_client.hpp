#pragma once
#include <QObject>
#include <QString>

class StreamlinkClient : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource)
    Q_PROPERTY(QString quality READ quality WRITE setQuality)

    QString _source, _quality;
    bool _updateQueued;
    int _fd;
    void scheduleUpdate();
    void start();
private slots:
    void update();
public:
    explicit StreamlinkClient(QObject *parent = nullptr);
    ~StreamlinkClient();
    QString source() const;
    void setSource(const QString& source);
    QString quality() const;
    void setQuality(const QString& quality);
signals:
    void accepted(const QString &url);
    void error(const QString &error);
};
