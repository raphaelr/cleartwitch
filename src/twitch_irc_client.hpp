#pragma once
#include <QObject>
#include <QString>
#include <QScopedPointer>

class TwitchIrcClient;
class TwitchIrcClientPrivate;
class TwitchIrcChannelPrivate;

class TwitchIrcChannel : public QObject {
    Q_OBJECT
    Q_PROPERTY(TwitchIrcClient* client READ client WRITE setClient);
    Q_PROPERTY(QString name READ name WRITE setName);
    Q_DECLARE_PRIVATE(TwitchIrcChannel)
    QScopedPointer<TwitchIrcChannelPrivate> d_ptr;
public:
    explicit TwitchIrcChannel(QObject* parent = nullptr);
    ~TwitchIrcChannel() override;
    QString name() const;
    void setName(QString name);
    TwitchIrcClient* client() const;
    void setClient(TwitchIrcClient* client);
public slots:
    void send(const QString& message);
private slots:
    void doUpdate();
signals:
    void joined();
    void messageReceived(const QString& message);
};

class TwitchIrcClient : public QObject {
    Q_OBJECT
    Q_DECLARE_PRIVATE(TwitchIrcClient)
    QScopedPointer<TwitchIrcClientPrivate> d_ptr;
public:
    explicit TwitchIrcClient(QObject* parent = nullptr);
    ~TwitchIrcClient() override;
    friend class TwitchIrcChannel;
};
