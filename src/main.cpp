#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "qqml.h"
#include "streamlink_client.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("tape software"); 
    QCoreApplication::setOrganizationDomain("tapesoftware.net"); 
    QCoreApplication::setApplicationName("clear twitch"); 

    qmlRegisterType<StreamlinkClient>("Cleartwitch", 1, 0, "StreamlinkClient");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
