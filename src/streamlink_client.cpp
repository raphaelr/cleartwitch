#include <string>
#include <unistd.h>
#include <QLoggingCategory>
#include <QDebug>
#include <QMetaObject>
#include "streamlink_client.hpp"

Q_LOGGING_CATEGORY(logStreamlinkClient, "StreamlinkClient")

StreamlinkClient::StreamlinkClient(QObject *parent)
    : QObject(parent)
    , _updateQueued(false)
    , _fd(-1)
{
}

StreamlinkClient::~StreamlinkClient()
{
    if (_fd >= 0) { close(_fd); }
}

QString StreamlinkClient::source() const { return _source; }
void StreamlinkClient::setSource(const QString& source)
{
    _source = source;
    scheduleUpdate();
}
QString StreamlinkClient::quality() const { return _quality; }
void StreamlinkClient::setQuality(const QString& quality)
{
    _quality = quality;
    scheduleUpdate();
}

void StreamlinkClient::scheduleUpdate()
{
    if (_updateQueued) { return; }
    _updateQueued = true;
    QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
}

void StreamlinkClient::update()
{
    qCInfo(logStreamlinkClient) << "update" << _source << _quality;
    _updateQueued = false;
    if (_fd >= 0) {
        close(_fd);
        _fd = -1;
    }
    if (_source.isEmpty() || _quality.isEmpty()) {
        emit accepted(QStringLiteral(""));
        return;
    }

    start();
}

void StreamlinkClient::start()
{
    int fds[2];
    if (pipe(fds)) {
        emit error(strerror(errno));
        return;
    }
    auto source = _source.toStdString();
    auto quality = _quality.toStdString();

    switch (fork()) {
    case -1:
        close(fds[0]); close(fds[1]);
        emit error(strerror(errno));
        break;
    case 0:
        dup2(fds[1], 1);
        close(fds[0]); close(fds[1]);
        execlp("streamlink", "streamlink", "--stdout", source.c_str(), quality.c_str(), nullptr);
        break;
    default: {
        close(fds[1]);
        _fd = fds[0];
        auto url = QStringLiteral("gst-pipeline: "
            "fdsrc fd=%1 ! "
            "decodebin use-buffering=true name=demux "
            "demux. ! audioconvert ! audioresample ! autoaudiosink "
            "demux. ! qtvideosink "
        ).arg(_fd);
        qCDebug(logStreamlinkClient) << "accepted" << url;
        emit accepted(url);
        break; }
    }
}
