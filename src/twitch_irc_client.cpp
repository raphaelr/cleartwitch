#include "twitch_irc_client.hpp"

class TwitchIrcChannelPrivate
{
    Q_DECLARE_PUBLIC(TwitchIrcChannel)
    TwitchIrcChannel* q_ptr;
public:
    TwitchIrcClient *_client, *_registeredClient;
    QString _name, _registeredName;
    bool _updateQueued;

    TwitchIrcChannelPrivate(TwitchIrcChannel *q);
    void queueUpdate();
};

class TwitchIrcClientPrivate
{
    Q_DECLARE_PUBLIC(TwitchIrcClient)
    TwitchIrcClient* q_ptr;
public:
    void addRef(TwitchIrcChannel *channel, const QString &name);
    void removeRef(TwitchIrcChannel *channel, const QString &name);
};

TwitchIrcChannelPrivate::TwitchIrcChannelPrivate(TwitchIrcChannel *q)
    : q_ptr(q)
    , _client(nullptr)
    , _registeredClient(nullptr)
    , _updateQueued(false)
{
}

void TwitchIrcChannelPrivate::queueUpdate()
{
    Q_Q(TwitchIrcChannel);
    if (_updateQueued) return;
    _updateQueued = true;
    QMetaObject::invokeMethod(q, "doUpdate", Qt::QueuedConnection);
}

TwitchIrcChannel::TwitchIrcChannel(QObject* parent)
    : QObject(parent)
    , d_ptr(new TwitchIrcChannelPrivate(this))
{
}
TwitchIrcChannel::~TwitchIrcChannel()
{
}

void TwitchIrcChannel::doUpdate()
{
    Q_D(TwitchIrcChannel);
    if (d->_registeredClient && !d->_registeredName.isNull()) {
        d->_registeredClient->d_ptr->removeRef(this, d->_registeredName);
    }
    if (d->_client && !d->_name.isNull()) {
        d->_client->d_ptr->addRef(this, d->_name);
    }
    d->_registeredClient = d->_client;
    d->_registeredName = d->_name;
    d->_updateQueued = false;
}

QString TwitchIrcChannel::name() const
{
    const Q_D(TwitchIrcChannel);
    return d->_name;
}
void TwitchIrcChannel::setName(QString name)
{
    Q_D(TwitchIrcChannel);
    d->_name = name;
    d->queueUpdate();
}
TwitchIrcClient* TwitchIrcChannel::client() const
{
    const Q_D(TwitchIrcChannel);
    return d->_client;
}
void TwitchIrcChannel::setClient(TwitchIrcClient* client)
{
    Q_D(TwitchIrcChannel);
    d->_client = client;
    d->queueUpdate();
}

TwitchIrcClient::~TwitchIrcClient()
{
}

void TwitchIrcClientPrivate::addRef(TwitchIrcChannel *channel, const QString &name)
{
}

void TwitchIrcClientPrivate::removeRef(TwitchIrcChannel *channel, const QString &name)
{
}
