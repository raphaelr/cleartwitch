import QtQml.Models 2.15 as Models
import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import QtMultimedia 5.15 as MM
import org.kde.kirigami 2.13 as Kirigami
import Cleartwitch 1.0 as App

Kirigami.Page {
    id: page
    property string source;
    property string quality;

    MM.MediaPlayer {
        id: player
    }

    MM.VideoOutput {
        anchors.fill: parent
        source: player
    }

    App.StreamlinkClient {
        id: streamlink
        source: page.source
        quality: page.quality
        onAccepted: function(url) {
            player.source = url;
            player.play();
        }
    }
}
