import QtQuick 2.6
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.OverlaySheet {
    id: sheet
    property ListModel model;

    header: Kirigami.Heading {
        text: "Add todo"
    }

    Kirigami.FormLayout {
        Controls.TextField {
            id: nameField
            Kirigami.FormData.label: "Name"
            placeholderText: "Something..."
        }
        Controls.Button {
            id: doneButton
            Layout.fillWidth: true
            text: "Add"
            icon.name: "list-add"
            enabled: nameField.text.length > 0

            onClicked: {
                model.append({
                    name: nameField.text,
                    done: false,
                });
                nameField.text = "";
                sheet.close();
            }
        }
    }
}
