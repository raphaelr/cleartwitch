# cleartwitch - clean twitch client

Work in progress.

## Dependencies

Qt 5 core, declarative, and widgets.

## Usage

    meson setup build
    ninja -C build
    ./build/cleartwitch

## License

cleartwitch - clean twitch client
Copyright (C) 2021 Raphael Robatsch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
